#ifndef M_M_READ_H
#define M_M_READ_H

#include <stdio.h>
#include <stdlib.h>
#include "mm_io.hpp"
#include "./../include/Sparse_Struct.hpp"
#include <iostream>
#include <vector>
#include <algorithm> 
#include <iomanip>

matSPRS readMarketMatrixCSR(const char *filename);

matSPRS readMarketMatrixRowCSR(const char *filename);

matSPRS readMarketMatrixCOO(const char *filename);

matSPRS coo_to_csr (const matSPRS mCOO);
#endif
