using namespace std;

#include "matrix_market_read.hpp"

matSPRS readMarketMatrixCSR(const char *filename){

    matSPRS mCSR;

    int ret_code;
    MM_typecode matcode;
    FILE *f; 
    int M, N, nz;   
  
    { 
        if ((f = fopen(filename, "r")) == NULL)
        {
			cout<<filename<<" FILE NOT FOUND\n"<<endl;
            exit(1);
		}
    }

    if (mm_read_banner(f, &matcode) != 0)
    {
        cout<<"Could not process matSPRS Market banner.\n";
        exit(1);
    }

    /* find out size of sparse matSPRS .... */
    if ((ret_code = mm_read_mtx_crd_size(f, &M, &N, &nz)) !=0)
        exit(1);

    /*********************************************************************/
    /* Read matSPRS Market file and Save matSPRS to A*/
    /*********************************************************************/
    vector<double> temp_Val(M,0);
    vector<int> temp_Row(M+1,0);
    int tmpCol, tmpRow, tmpsize=0;
    cout<<"Lecture du fichier:"<<endl;
    for (int i=0; i<nz; i++)
    {
		//cout<<"\r "<<(int)(((1.*i)/nz)*100)+1<<"%";
       // mCOO.Val.push_back(int());
        if(fscanf(f, "%d %d \n", &tmpRow, &tmpCol)!=2) // in matSPRS Market file Row before Coln
        {
			cout<<"Probable error while reading\n";
		}; 
        /* adjust from 1-based to 0-based */
        tmpRow--;
		tmpCol--;
		if(tmpRow==tmpCol)
		{
			cout<<"La matrice n'est pas une matrice d'adjacence\n";
			//wait();
		}
		temp_Val[tmpCol]+=1;
		vector<int>::iterator iter=mCSR.Col.begin();
		if(tmpsize==0)
		{
			mCSR.Col.push_back(tmpCol);
			tmpsize++;
		}
		else
		{
			mCSR.Col.insert(iter+temp_Row[tmpRow+1],tmpCol);
			tmpsize++;
		}
		for(int j=tmpRow+1;j<(M+1);j++)
		{
			temp_Row[j]+=1;
		}

    } 
    cout<<endl;
    mCSR.Row=temp_Row;
    mCSR.Val=temp_Val;

    if (f !=stdin) fclose(f);

    return mCSR;
}

matSPRS readMarketMatrixRowCSR(const char *filename){

    matSPRS mCSR;

    int ret_code;
    MM_typecode matcode;
    FILE *f; 
    int M, N, nz;   
  
    { 
        if ((f = fopen(filename, "r")) == NULL)
        {
			cout<<filename<<" FILE NOT FOUND\n"<<endl;
            exit(1);
		}
    }

    if (mm_read_banner(f, &matcode) != 0)
    {
        cout<<"Could not process matSPRS Market banner.\n";
        exit(1);
    }

    /* find out size of sparse matSPRS .... */
    if ((ret_code = mm_read_mtx_crd_size(f, &M, &N, &nz)) !=0)
        exit(1);

    /*********************************************************************/
    /* Read matSPRS Market file and Save matSPRS to A*/
    /*********************************************************************/
    vector<double> temp_Val(M,0);
    vector<int> temp_Row(M+1,0);
    vector<int> temp_Col(nz,0);
    int tmpCol, tmpRow,cptRow=0,savedRow=0;
    cout<<"Lecture du fichier:"<<endl;
    for (int i=0; i<nz; i++)
    {
		//cout<<"\r "<<(int)(((1.*i)/nz)*100)+1<<"%";
        //mCOO.Val.push_back(int());
        if(fscanf(f, "%d %d \n", &tmpRow, &tmpCol)!=2) // in matSPRS Market file Row before Coln
        {
			cout<<"Probable error while reading\n";
		}; 
        /* adjust from 1-based to 0-based */
        tmpRow--;
		tmpCol--;
		
		if(tmpRow==tmpCol)
		{
			cout<<"La matrice n'est pas une matrice d'adjacence\n";
			//wait();
		}
		
		if(i==0)
		{
			savedRow=tmpRow;
		}
		else if(savedRow != tmpRow)
		{
			for(int j=savedRow+1; j<=tmpRow;j++)
			{
				temp_Row[j]=cptRow;
			}
			savedRow=tmpRow;
		}
		cptRow++;
		temp_Col[i]=tmpCol;
		temp_Val[tmpCol]+=1;

    } 
    for(int j=savedRow+1; j<=M;j++)
	{
		temp_Row[j]=cptRow;
	}
    
    cout<<endl;
    mCSR.Row=temp_Row;
    mCSR.Val=temp_Val;
	mCSR.Col=temp_Col;

    if (f !=stdin) fclose(f);

    return mCSR;
}


matSPRS readMarketMatrixCOO(const char *filename){

    matSPRS mCOO, mCOO_r;

    int ret_code;
    MM_typecode matcode;
    FILE *f;
    int M, N, nz;   
  
    { 
        if ((f = fopen(filename, "r")) == NULL)
        {
			cout<<filename<<" FILE NOT FOUND\n"<<endl;
            exit(1);
		}
    }

    if (mm_read_banner(f, &matcode) != 0)
    {
        cout<<"Could not process matSPRS Market banner.\n";
        exit(1);
    }

    /* find out size of sparse matSPRS .... */
    if ((ret_code = mm_read_mtx_crd_size(f, &M, &N, &nz)) !=0)
        exit(1);

    /*********************************************************************/
    /* Read matSPRS Market file and Save matSPRS to A*/
    /*********************************************************************/
    std::vector<double> temp_Val(M,0);
    mCOO.Val=temp_Val;

    mCOO.Row.resize(nz);
    mCOO.Col.resize(nz);
    cout<<"Lecture de la matrice en COO\n";
    for (int i=0; i<nz; i++)
    {
		//cout<<"\r "<<(int)(((1.*i)/nz)*100)+1<<"%";
        if(fscanf(f, "%d %d \n", &mCOO.Row[i], &mCOO.Col[i])!=2) // in matSPRS Market file Row before Coln
        {
			cout<<"Probable error while reading\n";
		};  // in matSPRS Market file Col before Row
        /* adjust from 1-based to 0-based */
        mCOO.Col[i]--;
        mCOO.Row[i]--;
        mCOO.Val[mCOO.Col[i]]+=1;

    }
	cout<<"\n";
    if (f !=stdin) fclose(f);

    return mCOO;
}